﻿DROP DATABASE IF EXISTS opcion3;
CREATE DATABASE opcion3; 
USE opcion3;

CREATE TABLE ejemplar( 
  codejemplar int,
  PRIMARY KEY (codejemplar)
       );

CREATE TABLE socio (
   codsocio int,
   PRIMARY KEY (codsocio) 
);

CREATE TABLE  presta (
      cejemplar int,
      csocio int,
      fechaentrada date,
      fechasalida date,
     PRIMARY KEY (cejemplar,csocio),
     UNIQUE KEY (cejemplar),
     CONSTRAINT fkprestaejemplar FOREIGN KEY (cejemplar)
     REFERENCES ejemplar (codejemplar),
     CONSTRAINT fkprestasocio FOREIGN KEY (csocio)
     REFERENCES socio (codsocio)
);