DROP DATABASE IF EXISTS opcion6;
CREATE DATABASE opcion6; 
USE opcion6;


CREATE TABLE ejemplar (
   codejemplar int,
   PRIMARY KEY (codejemplar) 
);

CREATE TABLE socio (
      cejemplar int,
      codsocio int,
      fechaentrada date,
      fechasalida date,
     PRIMARY KEY (codsocio),
     CONSTRAINT fkejemplarsocio FOREIGN KEY (cejemplar)
     REFERENCES ejemplar (codejemplar)
);