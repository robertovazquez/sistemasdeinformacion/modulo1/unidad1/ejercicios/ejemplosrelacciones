﻿DROP DATABASE IF EXISTS opcion5;
CREATE DATABASE opcion5; 
USE opcion5;


CREATE TABLE socio (
   codsocio int,
   PRIMARY KEY (codsocio) 
);

CREATE TABLE ejemplar (
      codejemplar int,
      csocio int,
      fechaentrada date,
      fechasalida date,
     PRIMARY KEY (codejemplar),
     CONSTRAINT fksocioejemplar FOREIGN KEY (csocio)
     REFERENCES socio (codsocio)
);