DROP DATABASE IF EXISTS opcion4;
CREATE DATABASE opcion4; 
USE opcion4;

CREATE TABLE ejemplar( 
  codejemplar int,
  PRIMARY KEY (codejemplar)
       );

CREATE TABLE socio (
   codsocio int,
   PRIMARY KEY (codsocio) 
);

CREATE TABLE  presta (
      cejemplar int,
      csocio int,
      fechaentrada date,
      fechasalida date,
     PRIMARY KEY (cejemplar,csocio),
     UNIQUE KEY (cejemplar),
     UNIQUE KEY(csocio),
     CONSTRAINT fkprestaejemplar FOREIGN KEY (cejemplar)
     REFERENCES ejemplar (codejemplar),
     CONSTRAINT fkprestasocio FOREIGN KEY (csocio)
     REFERENCES socio (codsocio)
);