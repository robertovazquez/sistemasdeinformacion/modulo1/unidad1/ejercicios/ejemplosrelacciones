
DROP DATABASE IF EXISTS b05062019;
CREATE DATABASE  b05062019 ; 
USE b05062019 ;

CREATE OR REPLACE TABLE personas ( 
  dni varchar(10),
  nombre varchar (100),
  
 PRIMARY KEY (dni)
       );
INSERT INTO personas (dni,nombre)
  VALUES
        ('d1', 'n1'),
        ('d2','n2');

CREATE OR REPLACE TABLE  telefonos (
   dniper varchar(10),
   numerotelefono varchar (12),
   PRIMARY KEY (dniper,numerotelefono),
   CONSTRAINT fktelefonospersonas FOREIGN KEY (dniper)
   REFERENCES personas (dni)
);
INSERT INTO  telefonos (dniper,numerotelefono)
  VALUES
       ('d1', 't1'),
       ('d1','t2'),
       ('d2','t3');