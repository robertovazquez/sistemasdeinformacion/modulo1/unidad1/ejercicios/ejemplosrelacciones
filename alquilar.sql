﻿--alquiler de peliculas


DROP DATABASE IF EXISTS peliculas;
CREATE DATABASE peliculas;
USE peliculas;

CREATE OR REPLACE TABLE socios(
  codigosocio varchar(15),
  nombre varchar (20),
  PRIMARY KEY (codigosocio)

);

CREATE OR REPLACE TABLE pelicula (

   codigopelicula varchar (15),
   titulo varchar (30),
   PRIMARY KEY (codigopelicula)

);

CREATE OR REPLACE TABLE telefonos(
  codigoso varchar (15),
  telefono varchar (20),
  PRIMARY KEY (codigoso,telefono),
  CONSTRAINT fktelefonosocios FOREIGN KEY (codigoso)
  REFERENCES socios (codigosocio)

);

CREATE OR REPLACE TABLE email(
  codigoso varchar (15),
  email varchar (20),
  PRIMARY KEY (codigoso,email),
  CONSTRAINT fkemailsocios FOREIGN KEY (codigoso)
  REFERENCES socios (codigosocio)

);

CREATE OR REPLACE TABLE alquila (
  codigoso varchar (15),
  codigope varchar (15),
  PRIMARY KEY (codigoso,codigope),
  CONSTRAINT fkalquilasocios FOREIGN KEY (codigoso)
  REFERENCES socios (codigosocio),
  CONSTRAINT fkalquilapelicula FOREIGN KEY (codigope)
  REFERENCES pelicula (codigopelicula)

);

CREATE OR REPLACE TABLE fecha (
   codigoso varchar (15),
   codigope varchar (15),
   fecha date,
   PRIMARY KEY (codigoso,codigope,fecha),
  CONSTRAINT fkfechaalquila FOREIGN KEY (codigoso,codigope)
  REFERENCES alquila (codigoso,codigope)
  
);

   

