﻿-- 1 n

DROP DATABASE IF EXISTS opcion2;
CREATE DATABASE opcion2; 
USE opcion2;

CREATE TABLE ejemplar( 
  codejemplar int,
  PRIMARY KEY (codejemplar)
       );

CREATE TABLE socio (
   codsocio int,
   PRIMARY KEY (codsocio) 
);

CREATE TABLE  presta (
      cejemplar int,
      csocio int,
      fechaentrada date,
      fechasalida date,
     PRIMARY KEY (cejemplar,csocio),
     UNIQUE KEY (csocio),
     CONSTRAINT fkprestaejemplar FOREIGN KEY (cejemplar)
     REFERENCES ejemplar (codejemplar),
     CONSTRAINT fkprestasocio FOREIGN KEY (csocio)
     REFERENCES socio (codsocio)
);