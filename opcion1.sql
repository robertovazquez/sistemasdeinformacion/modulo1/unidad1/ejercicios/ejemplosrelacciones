
-- n n

DROP DATABASE IF EXISTS opcion1;
CREATE DATABASE opcion1; 
USE opcion1;

CREATE OR REPLACE TABLE ejemplar( 
  codejemplar int,
  PRIMARY KEY (codejemplar)
       );

CREATE OR REPLACE TABLE socio (
   codsocio int,
   PRIMARY KEY (codsocio) 
);

CREATE OR REPLACE TABLE  presta (
      cejemplar int,
      csocio int,
      fechaentrada date,
      fechasalida date,
     PRIMARY KEY (cejemplar,csocio),
     CONSTRAINT fkprestaejemplar FOREIGN KEY (cejemplar)
     REFERENCES ejemplar (codejemplar),
     CONSTRAINT fkprestasocio FOREIGN KEY (csocio)
     REFERENCES socio (codsocio)
);
 
